//Robert de Bruyn Author

window.onload = function() {
    
    var toGal = document.getElementById('toGallery');
    toGal.onclick = function (){
        location.href = "gallery.html";
    }
    
    
    
    var canvas = document.getElementById('mainField');

    var ctx = canvas.getContext('2d');

    //can put code here for dynamic size?
    //jk its below


    var mouse = {x: 0, y: 0};

    // mouse capture

    canvas.addEventListener('mousemove', function(e){
        mouse.x = e.pageX - this.offsetLeft;
        mouse.y = e.pageY - this.offsetTop;
    }, false);


    //paint part

    ctx.lineWidth = 2;
    ctx.lineJoin = 'round';
    ctx.lineCap = 'round';
    ctx.strokeStyle = 'black';

    canvas.addEventListener('mousedown', function(e){
        ctx.beginPath();
        ctx.moveTo(mouse.x, mouse.y);

        canvas.addEventListener('mousemove', onPaint, false);
    }, false);
    
    canvas.addEventListener('mouseup', function(){
        canvas.removeEventListener('mousemove', onPaint, false);
    }, false);

    var onPaint = function(){
        ctx.lineTo(mouse.x, mouse.y);
        ctx.stroke();
    };
    
    //sidebar elements
    //paint colour/size
    
    var btnRed = document.getElementById('red');
    btnRed.onclick = function (){
        ctx.strokeStyle = 'red';
        };
    var btnYellow = document.getElementById('yellow');
    btnYellow.onclick = function (){
        ctx.strokeStyle = 'yellow';
        };
    var btnBlue = document.getElementById('blue');
    btnBlue.onclick = function (){
        ctx.strokeStyle = 'blue';
        };
    var btnPurple = document.getElementById('purple');
    btnPurple.onclick = function (){
        ctx.strokeStyle = 'purple';
        };
    var btnOrange = document.getElementById('orange');
    btnOrange.onclick = function (){
        ctx.strokeStyle = 'orange';
        };
    var btnGreen = document.getElementById('green');
    btnGreen.onclick = function (){
        ctx.strokeStyle = 'green';
        };
    var btnWhite = document.getElementById('white');
    btnWhite.onclick = function (){
        ctx.strokeStyle = 'white';
        };
    var btnBlack = document.getElementById('black');
    btnBlack.onclick = function (){
        ctx.strokeStyle = 'black';
        };
    
    var btnTwo = document.getElementById('2');
    btnTwo.onclick = function (){
        ctx.lineWidth = 2;
        };
    var btnFive = document.getElementById('5');
    btnFive.onclick = function (){
        ctx.lineWidth = 5;
        };
    var btnTen = document.getElementById('10');
    btnTen.onclick = function (){
        ctx.lineWidth = 10;
        };
    var btnTwenty = document.getElementById('20');
    btnTwenty.onclick = function (){
        ctx.lineWidth = 20;
        };
    var btnUDS = document.getElementById('goPoint');
    var udSize = document.getElementById('diff');
    btnUDS.onclick = function (){
        ctx.lineWidth = udSize.value;
    }
    
    // "Dynamic" size of canvas
    var btnSelSize = document.getElementById('goSize');
    var udHeight = document.getElementById('selHeight');
    var udWidth = document.getElementById('selWidth');
    btnSelSize.onclick = function (){
        var tempCanvas = document.createElement('canvas');
        tempCanvas.height = canvas.height;
        tempCanvas.width = canvas.width;
        
       
tempCanvas.getContext('2d').drawImage(canvas, 0, 0);
        if(udHeight.value == "taco"){
            console.log('lol unknown height')
        }else{
            canvas.height = udHeight.value;
        }
        if(udWidth.value == "taco")
        {
            console.log('lol unknown width')
        }else{
            canvas.width = udWidth.value;
        }
            
        canvas.getContext('2d').drawImage(tempCanvas, 0, 0, tempCanvas.width, tempCanvas.height);
        
    }
    var btnSubmitImage = document.getElementById('submit');
    btnSubmitImage.onclick = function (){
        var stupid = document.getElementById('mainField');
        var dataURL = stupid.toDataURL("image/png");
        
        var names = document.getElementById('tags');
        var name = names.value;
        
        document.getElementById('hidden_data').value = dataURL;
        var fd = new FormData(document.forms["form1"]);
        //document.getElementById('tags').value = dataURL2;
        //var tags = new FormData(document.forms["form0"]);
        console.log(fd);
        console.log( tags);
        
        //alert(postdata);
        
        var xhr = new XMLHttpRequest();
        
        xhr.open('POST', 'src/insert.php', false);
        //ajax.setRequestHeader('Content-Type', 'application/upload');
        
         xhr.upload.onprogress = function(e) {
                    if (e.lengthComputable) {
                        var percentComplete = (e.loaded / e.total) * 100;
                        console.log(percentComplete + '% uploaded');
                        alert('Succesfully uploaded');
                    }
                };
 
                xhr.onload = function() {
 
                };
        fd.append("name",name);
        //var params = (fd + "&tags=" + tags)
        
        xhr.send(fd);
        //console.log(fd + '&' + tags);
        
        var messagebar = document.getElementById('message');
        messagebar.innerHTML = ('Image Submission Request Sent')
        
 
    };
    
    
    
};



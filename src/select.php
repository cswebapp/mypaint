<?php 
//Bradly Hamilton & Nicholas Casbarro Authors

    //header('Content-Type: text/xml');
    // todo: obtain data from html

    // change username and password below to what your phpmyadmin config file says


    $tags = "pic";
        

    $servername = "localhost";
    $username = "root";
    $password = "";
    $database = "projectDB";

    // Create connection
    try {
        $conn = new PDO("mysql:host=$servername; dbname=$database", $username, $password);

        // obtain image links into database
        $sql = $conn->prepare("SELECT name,link FROM info WHERE ? LIKE name;");
        $la=$tags;
        $sql->bindValue(1, $la, PDO::PARAM_STR);
        $sql->execute();
        $result = $sql->fetchAll();
        $rowCount = count($result);
    }
    catch (PDOException $e) {
        echo $e->getMessage();
    }
    echo "<results>";
    if ($rowCount > 0) {
        // output data of each row
        for ($i=0;$i<$rowCount;$i++) {
            echo "<image source='".$result[$i][1]."' "."name='".$result[$i][0]."'/>";
        }
    } else {
        echo "<image source='!!!'/>";
    }

    echo "</results>";

    $conn = null;
?>
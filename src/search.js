//Nicholas Casbarro Author

//could not manage to get query to pass to php file

function getData() {
    $.ajax({
        type: "POST",
        url: "src/select.php",
        data: { searchText : $("div#searchBar").val() },
        dataType: "xml",
        success: resultsInsert
    });
}
function resultsInsert(xml) {
    $("div#gallery").empty();
    //loop through and get xml results
    $(xml).find("image").each(function () {
        
        var source = $(this).attr("source");
        
        if (source == '!!!') {
            $("div#gallery").append($("<div>No results!</div>"));
        }
        else {
            //new image box template
            var box = $("<div class='box'>");
            var inner = $("<div class='boxInner'>");

            //source of image
            var imgStr = "<img src='upload/"+source+"'/>";
            var img = $(imgStr);
            inner.append(img);

            //captions of image
            var name = $(this).attr("name");
            var caption = $("<div class='caption'>");
            caption.text(name);
            inner.append(caption);

            //append it
            box.append(inner);
            $("div#gallery").append(box);
        }
    });
    //end loop
}